CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation


INTRODUCTION
------------

This is a very simple module that lets you add the entire vertical tab 
components as a panels content type. Usually the individual components 
of the vertical tab in node add/edit form can be added but then it looses
the tabified structure.

Here a single panels content type "Vertical Tab Elements" is available 
and it can be placed anywhere in the panels with the tabified structure
intact.

INSTALLATION
------------

1. Download and Enable the module at Administer >> Site building >> Modules.

2. In any panels "do add more" on any pane.

3. Select the "forms" group and select "vertical tab elements".

4. That's it. :) Drag and drop to any pane and still have the vertical 
   tabs structure intact. Hooray!
